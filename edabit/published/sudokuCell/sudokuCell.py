class Cell:
    def __init__(self, val, rLocation):
        self.val = val
        self.rLocation = rLocation

        # automatically create new index formats
        self.convIndices()

    # convert from JSON indices
    # to row, col, and box indices
    def convIndices(self):
        # get the object's rLocation
        (r0, r1) = self.rLocation

        # convert to col indices
        self.cLocation = (r1, r0)

        # convert to box indices
        b0 = r0
        b1 = r1

        while r0 > 2:
            r0 -= 3

        if r0 == 0:
            if b1 > 5:
                b0 += 2
                b1 -= 6
            elif b1 > 2:
                b0 += 1
                b1 -= 3
        elif r0 == 1:
            if b1 < 3:
                b0 -= 1
                b1 += 3
            elif b1 > 5:
                b0 += 1
                b1 -= 3
        else:
            if b1 < 3:
                b0 -= 2
                b1 += 6
            elif b1 < 6:
                b0 -= 1
                b1 += 3

        self.bLocation = (b0, b1)

    # get the cell's region
    def getRegions(self, regionData):
        # get row, col, box indices
        r0 = self.rLocation[0]
        c0 = self.cLocation[0]
        b0 = self.bLocation[0]

        # get the list of numbers that
        # are in the cell's region
        rowRegion = regionData[0][r0]
        colRegion = regionData[1][c0]
        boxRegion = regionData[2][b0]

        # remove the cell from the region
        rowRegion = [c for c in rowRegion if c.rLocation != self.rLocation]
        colRegion = [c for c in colRegion if c.rLocation != self.rLocation]
        boxRegion = [c for c in boxRegion if c.rLocation != self.rLocation]

        self.regions = [rowRegion, colRegion, boxRegion]

def getRegionData(puzzle):
    # initialize empty data structures
    # for row, col, and box data
    rowData = [[0] * 9 for _ in range(9)]
    colData = [[0] * 9 for _ in range(9)]
    boxData = [[0] * 9 for _ in range(9)]

    # make copies of the sudoku puzzle
    # into three different formats:
    # row, col, and box lists
    for (r0, row) in enumerate(puzzle):
        for (r1, n) in enumerate(row):
            # get the index and number
            rLocation = (r0, r1)
            n = puzzle[r0][r1]

            # create cell object with
            # different index formats
            cell = Cell(n, rLocation)

            # get index format attributes
            (c0, c1) = cell.cLocation
            (b0, b1) = cell.bLocation

            # put the number in new data structures
            rowData[r0][r1] = cell
            colData[c0][c1] = cell
            boxData[b0][b1] = cell

    return [rowData, colData, boxData]