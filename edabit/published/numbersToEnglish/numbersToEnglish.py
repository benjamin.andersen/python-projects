def numbersToEnglish(num):
    # check for valid inputs
    if not (-1e+93 < num < 1e+93):
        raise Exception

    if not num:
        return 'zero'

    if num < 0:
        # make negative number positive
        # and add prefix 'negative'
        negative = True
        num *= -1

    else:
        negative = False

    # make a string version of num
    # and list version to split num
    # into groups of three
    num = str(num)
    splitNum = [int(n) for n in num]
    splitNum.reverse()

    # group suffixes
    suffixes = [
    '',
    ' thousand',
    ' million',
    ' billion',
    ' trillion',
    ' quadrillion',
    ' quintillion',
    ' sextillion',
    ' septillion',
    ' octillion',
    ' nonillion',
    ' decillion',
    ' undecillion',
    ' duodecillion',
    ' tredecillion',
    ' quattuordecillion',
    ' quindecillion',
    ' sexdecillion',
    ' septendecillion',
    ' octodecillion',
    ' novemdecillion',
    ' vigintillion',
    ' unvigintillion',
    ' duovigintillion',
    ' trevigintillion',
    ' quattuorvigintillion',
    ' quinvigintillion',
    ' sexvigintillion',
    ' septenvigintillion',
    ' octovigintillion',
    ' novemvigintillion'
    ]

    # builder words
    ones = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
    teens = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
    tens = ['', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']

    # number of groups of three
    n = len(num) // 3

    # group the numbers into threes backwards
    for i in range(n):
        splitNum.append(splitNum[:3])
        del splitNum[:3]

    # add the last group and delete
    # non-grouped numbers for
    if len(num) % 3:
        if n:
            splitNum = splitNum[-n:] + [splitNum[:-n]]

        # number < 100, so wrap
        # the number in a list
        else:
            splitNum = [splitNum]

    # build the word representation
    for (i, group) in enumerate(splitNum):
        # only process groups with a prefix
        if (group != [0, 0, 0]):
            # format groups that are in the 'teens'
            if len(group) >= 2 and group[1] == 1:
                group[1] = '1{0}'.format(group[0])
                group[0] = 0

            for (j, n) in enumerate(group):
                # ones place
                if not j:
                    group[j] = ones[n]

                # teens / tens place
                elif j == 1:
                    if type(n) == str:
                        group[j] = teens[int(n[1])]

                    else:
                        group[j] = tens[n]

                # hundreds place
                else:
                    if not n:
                        group[j] = ''

                    else:
                        group[j] = ones[n] + ' hundred'

            # re-order group
            group.reverse()

            # remove empty strings
            group = [w for w in group if w]

            # change the numeric representation to
            # the word representation
            prefix = ' '.join(group).strip()
            splitNum[i] = prefix + suffixes[i]

    # re-order num
    splitNum.reverse()

    # remove empty lists
    splitNum = [item for item in splitNum if type(item) == str]

    # format output
    words = ', '.join(splitNum).strip()

    # if negative
    if negative:
        words = 'negative ' + words

    return words

# import random

# testVals = []

# with open(r'C:\Users\ElohmroW\Documents\Programming\Python\GitLab Projects\edabit\published\numbersToEnglish\testVals.txt', 'w') as f:
#     for _ in range(98):
#         x = random.randint(-1e+93 + 1, 1e+93 - 1)

#         val = (x, numbersToEnglish(x))

#         testVals.append(val)

#     f.write(str(testVals))