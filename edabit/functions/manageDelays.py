def manageDelays(train, dest, delay):
    # only delay trains with the correct destination
    if dest in train.destinations:
        # get the time in hours and minutes
        time = train.expectedTime
        hour = int(time[:2])
        mins = int(time[3:]) + delay

        # truncate mins >= 60
        while mins >= 60:
            mins -= 60
            hour += 1

        # add extra 0 to mins if < 10
        if mins < 10:
            mins = '0' + str(mins)

        # only consider [0, 24], add extra 0 (as above)
        if (hour := hour % 24) < 10:
            hour = '0' + str(hour)

        # update expectedTime
        train.expectedTime = '{0}:{1}'.format(hour, mins)