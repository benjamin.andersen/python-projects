import re

def translateWord(word):
    if not word:
        return word

    vowels = {'a', 'e', 'i', 'o', 'u'}
    word = [c for c in word]

    # check for a vowel at the start
    if word[0].lower() in vowels:
        suffix = ['y']

    # translate the word
    else:
        suffix = []; prefix = []; j = 0
        capitalized = False

        for _ in range(len(word)):
            c = word[j]

            # check if letter is a consonant
            if (_c := c.lower()) not in vowels:
                # add to suffix, remove letter
                suffix.append(_c)
                word.pop(j)

                # preserve capitalization
                if not capitalized and c.isupper():
                    capitalized = True

            # skip in-word punctuation
            elif c == "'":
                j += 1

            else: break

    word = prefix + word + suffix + ['a', 'y']
    word = ''.join(word)

    if capitalized:
        word = word.capitalize()

    return word

def translateSentence(sentence):
    # ([alpha and ']+) [non-alpha]*
    # keep the () part, ignore the rest
    p = re.compile(r"([a-zA-Z']+)[^a-zA-Z]*")

    # find each match, then translate each word and replace capturing group
    return p.sub(replaceGroup, sentence)

def replaceGroup(m):
    # translate the word
    word = m.group(1)
    match = [c for c in m.group(0)]

    # replace the capturing group
    match[:m.end(1) - m.start(1)] = translateWord(word)
    return ''.join(match)