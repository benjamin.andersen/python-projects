import re

def markdown(symb):
    def formatter(strg, subStrg):
        return re.sub(r"%s[^a-z ]*" % subStrg, symb + r'\g<0>' + symb, strg, flags = re.I)

    return formatter