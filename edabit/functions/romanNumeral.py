def romanNumeral(n):
    # only convert numbers less than 4000
    if n < 4000:
        # all roman numerals categorized by place value
        romanNumerals = (
            ('I', 'V', 'X'),
            ('X', 'L', 'C'),
            ('C', 'D', 'M')
        )

        # create list of digits, then reverse
        n = [int(x) for x in str(n)]
        n.reverse()

        # build each numeral
        for (i, x) in enumerate(n):
            # place value < 1000
            if i < 3:
                # get the roman numerals for place value
                nums = romanNumerals[i]

                # additive notation
                if x < 4:
                    x *= nums[0]

                # subtractive notation
                elif x == 4:
                    x = nums[0] + nums[1]

                # additive notation
                elif x < 9:
                    x = nums[1] + ((x - 5) * nums[0])

                # subtractive notation
                else:
                    x = nums[0] + nums[2]

            # thousands place value
            else:
                x *= 'M'

            # change list val
            n[i] = x

        # reverse and join into string
        n.reverse()
        n = ''.join(n)

    return n