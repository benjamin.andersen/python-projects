import re

def validName(name):
    # use a regex to capture initials and words
    initials = re.findall(r'[A-Z]\.', name)
    words = re.findall(r'[A-Z][a-z]+', name)

    # combine initials and words into new list
    # and check to see if the list is long enough
    splitName = initials + words
    nameLen = len(splitName)

    # name can only be two or three long
    if nameLen in [2, 3]:
        # sort the substrings by the original
        # string's index value
        splitName.sort(key = lambda substr: name.find(substr))

        # get firstName, lastName, and the
        # joined regex name
        firstName = splitName[0]
        lastName = splitName[-1]
        joinedName = ' '.join(splitName)

        # make sure the name contains a valid pattern
        # and that the last name is a word
        if joinedName == name and not lastName in initials:
            if nameLen == 3:
                # get middleName
                middleName = splitName[1]

                # the name is invalid if firstName is
                # an initial and middleName is a word
                if firstName in initials:
                    if middleName in initials:
                        return True

                # this is always true when
                # firstName is a word
                else:
                    return True

            # this is always true when
            # name is two long
            else:
                return True

    return False