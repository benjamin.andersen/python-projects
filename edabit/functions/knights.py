def knights(board):
    possibleMoves = []

    # get board dimensions m x n
    m = len(board)
    n = len(board[0])

    # get all of the possible moves
    # for every knight on the board
    for (i, row) in enumerate(board):
        for (j, square) in enumerate(row):
            if square:
                # each knight has eight moves
                # described below
                moves = [
                (i - 1, j - 2),
                (i - 2, j - 1),
                (i + 1, j - 2),
                (i + 2, j - 1),
                (i - 1, j + 2),
                (i - 2, j + 1),
                (i + 1, j + 2),
                (i + 2, j + 1)
                ]

                possibleMoves.extend(moves)

    # remove all duplicate indices
    # and indices that are
    # outside of board dimensions
    possibleMoves = {(i, j) for (i, j) in possibleMoves if 0 <= i < m and 0 <= j < n}

    # look to see if a knight is
    # at any possible move
    for (i, j) in possibleMoves:
        if board[i][j]:
            return False

    return True