import json

########## VARIABLE DECLARATIONS ##########

data = []
paths = []
branchNodes = []

########## FUNCTIONS ##########

def expandUniquePathTree(currentTree, visitedNodes):
    ((rootNode, rootDistance), nodes) = currentTree
    _nodes = [_node for (_node, _distance) in nodes]

    if not _nodes:
        newTree = currentTree

    elif endNode in _nodes:
        for (leafNode, leafDistance) in nodes:
            if leafNode == endNode:
                leafNode = (endNode, leafDistance)
                break

        newTree = ((rootNode, rootDistance), [(leafNode, [])])

    else:
        currentNodes = _nodes + [rootNode]
        visitedNodes += currentNodes

        for (i, (node, distance)) in enumerate(nodes):
            nextNodes = data[node]
            nextNodes = [(nextNode, nextDistance) for (nextNode, nextDistance) in nextNodes if nextNode not in visitedNodes]
            branch = ((node, distance), nextNodes)
            (nodes[i], visitedNodes) = expandUniquePathTree(branch, visitedNodes)

        newTree = ((rootNode, rootDistance), nodes)
        visitedNodes = visitedNodes[:-len(currentNodes)]

    return newTree, visitedNodes

def getPaths(pathTree, path):
    (root, nodes) = pathTree
    path.append(root)

    if not nodes:
        if root[0] == endNode:
            paths.append(path)

    else:
        if len(nodes) > 1:
            branchNodes.append(root)

        for node in nodes:
            path = getPaths(node, path)

        if len(branchNodes) > 1 and root == branchNodes[-1]:
            del branchNodes[-1]

    if branchNodes:
        _path = path[:]
        _path.reverse()

        for (i, node) in enumerate(_path):
            if node == branchNodes[-1]:
                path = _path[i:]
                break

        path.reverse()

    return path

########## INPUT ##########

print()

while True:
    rawJSON = input('Provide the road graph JSON file: ')

    try:
        rawJSON = open(rawJSON)
        JSON = json.load(rawJSON)
        break

    except:
        print('Error: Invalid JSON file.')

while True:
    startNode = input('Provide the starting node: ')

    try:
        startNode = int(startNode)
        break

    except:
        print('Error: Invalid starting node.')

while True:
    endNode = input('Provide the destination node: ')

    try:
        endNode = int(endNode)

        if endNode == startNode:
            print('Error: Destination node is the same as the starting node.')

        else:
            break

    except:
        print('Error: Invalid destination node.')

########## PROCESSING ##########

nodes = JSON['nodes']
edges = JSON['edges']
nNodes = len(nodes)

for node in range(nNodes):
    targets = []

    for edge in edges:
        source = edge['source']
        target = edge['target']
        distance = edge['metadata']['distance']

        if source == node:
            targets.append((target, distance))

        elif target == node:
            targets.append((source, distance))

    data.append(targets)

initialTree = ((startNode, 0), data[startNode])
(pathTree, _) = expandUniquePathTree(initialTree, [])
getPaths(pathTree, [])

for (i, path) in enumerate(paths):
    newPath = list(zip(*path))
    path = list(newPath[0])
    distance = sum(newPath[1])
    paths[i] = (path, distance)

minDistance = [paths[0][1], 0]

for (i, (path, distance)) in enumerate(paths[1:]):
    if distance < minDistance[0]:
        minDistance = [distance, i + 1]

minIndex = minDistance[1]
(minPath, minDistance) = paths[minIndex]

########## OUTPUT ##########

print('\nThe minimum path from {0} -> {1} is {2} and occurs at a distance of {3}.\n'.format(startNode, endNode, minPath, minDistance))