import string

########## INPUT ##########

print()

while True:
    message = input('The message to be encrypted / decrypted: ')
    lMessage = [c for c in message if c.isalpha()]

    if not message:
        error = 'Error: No message provided.'

    elif not lMessage:
        error = 'Error: Invalid input for message.'

    else:
        error = None

    if error:
        print(error)

    else:
        message = ''.join(lMessage) # join list into string

        if message.isupper():
            cipherType = 'decrypt'

        else:
            cipherType = None
            message = message.upper()

        break

while True:
    if cipherType == 'decrypt':
        keyWord = input('The keyword to decrypt with: ')

    else:
        keyWord = input('The keyword to encrypt with: ')

    lKeyWord = [c for c in keyWord if c.isalpha()]

    if not keyWord:
        error = 'Error: No keyword provided'

    elif ' ' in keyWord:
        error = 'Error: Keyword cannot include whitespace.'

    elif not lKeyWord:
        error = 'Error: Invalid input for keyword.'

    else:
        error = None

    if error:
        print(error)

    else:
        keyWord = ''.join(lKeyWord).upper() # join list into string then capitalize
        break

########## VARIABLE DECLARATIONS ##########

alpha = [c for c in string.ascii_uppercase]
fullAlpha = alpha + alpha # duplicate alpha so we can get letters past z ['a', ... ,'z', 'a', ... , 'z']

########## PROCESSING ##########

vignereCipher = {}

for i, c1 in enumerate(alpha):
    for j, c2 in enumerate(alpha):
        if cipherType == 'decrypt':
            n = i - j

            if n < 0:
                c_ = fullAlpha[n + 26]

            else:
                c_ = fullAlpha[n]

        else:
            n = i + j
            c_ = fullAlpha[n]

        vignereCipher.update({'{0}{1}'.format(c1, c2): c_})

messageLen = len(message)

# repeat the keyword
while len(keyWord) < messageLen:
    keyWord += keyWord

# remove the excess letters
while len(keyWord) > messageLen:
    keyWord = keyWord[:-1]

# encrypt / decrypt the message using the key
newMessage = ''
keyWord = [c for c in keyWord]
message = [c for c in message]
zipL = list(zip(message, keyWord))

for c1, c2 in zipL:
    newChar = vignereCipher['{0}{1}'.format(c1, c2)]
    newMessage += newChar

########## OUTPUT ##########

if cipherType == 'decrypt':
    print('\nThe decrypted messaged is: {0}\n'.format(newMessage))

else:
    print('\nThe encrypted messaged is: {0}\n'.format(newMessage))