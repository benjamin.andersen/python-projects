import collections

########## INPUT ##########

print()

while True:
    n = input('Which nth ulam number would you like? ')

    try:
        n = int(n)
        error = False

    except:
        error = True

    if error or n <= 0:
        print('Error: Invalid input.')

    else:
        break

########## VARIABLE DECLARATIONS ##########

ulam = [1,2]

########## FUNCTIONS ##########

def nextUlam(ulam):
    sums = []
    distinctSums = []
    for i, x in enumerate(ulam):
        newUlam = ulam[i:] # create a new changing list to eliminate duplicates (ie. x = 2, y = 1 is the same as x = 1, y = 2)

        for y in newUlam:
            if x != y: # the two integers must be distinct
                sums.append(x + y) # list of the sums of two distinct integers

    for sumsSum, occurrance in collections.Counter(sums).items(): # find all distinct sums
        if occurrance == 1: # a distinct sum occurs once
            distinctSums.append(sumsSum)

    distinctSums = [dSum for dSum in distinctSums if dSum > ulam[-1]] # delete all distinct sums that are less than or equal to the last ulam number in the current sequence
    newUlam = min(distinctSums) # find the new ulam number (the smallest sum in the list of distinct sums)
    return newUlam

########## PROCESSING ##########

if n > 2:
    for _ in range(n-2): # number of iterations is n minus the initial two Ulum numbers
        newUlam = nextUlam(ulam) # get the next ulam number
        ulam.append(newUlam) # update the list of ulam numbers

########## OUTPUT ##########

print('\nThe {0}th ulam number is: {1}\n'.format(n, ulam[n-1]))