########## VARIABLE DECLARATIONS ##########

validChars = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',' ']

########## INPUT ##########

print('----------INPUT----------\n')

while True:
    userInput = input('Please input a set of words separated by spaces: ')
    userInput = userInput.lower() # lowercase the input for error checking

    for c in userInput: # check each character to see if it is valid
        if c not in validChars: # raise error and break loop
            error = 'Error: Invalid character in input.'
            break

        else:
            error = None

    if error:
        print(error)

    else:
        break

########## PROCESSING ##########

words = userInput.split() # split the input into a list of words
wordSets = []
nWords = len(words) # get the number of words
nOverlaps = [] # initiate empty list to find the minimum overlap

for i in range(nWords - 1): # the number of word sets is equal to nWords - 1
    firstWord = list(reversed([c for c in words[i]])) # create a reversed list of characters corresponding to the first word
    secondWord = [c for c in words[i + 1]] # create a list of characters corresponding to the second word
    jMax = len(firstWord) # the j iterator can only be less than the length of the first word initially
    kMax = len(secondWord) # the j iterator can only be less than the length of the second word
    wordSets.append([firstWord, secondWord, jMax, kMax]) # build the list of word sets

for i, wordSet in enumerate(wordSets):
    firstWord = wordSet[0]
    secondWord = wordSet[1]
    jMax = wordSet[2]
    kMax = wordSet[3]
    FWlen = len(firstWord)
    SWLen = len(secondWord)
    overlap = ''

    if FWlen <= SWLen: # the number of iterations is equal to the length of the shorter word
        n = FWlen

    else:
        n = SWLen

    j = 0; k = 0

    while j < jMax and k < kMax:

        FWchar = firstWord[j]
        SWchar = secondWord[k]

        if FWchar == SWchar: # if the two characters overlap
            overlap += FWchar # add the overlapping character to the string
            jMax = j # j can only be smaller than the last time an overlapping character was found
            j = 0 # reset dynamic iterator
            k += 1 # increment static iterator

        else:
            j += 1

    nOverlap = len(overlap) # find the number of overlapping characters
    nOverlaps.append(nOverlap) # append the overlap number
    firstWord = list(reversed(firstWord)) # reverse the first word
    secondWord = secondWord[nOverlap:] # remove the overlapping characters from the first word

    if i == 0:
        newWord = firstWord + secondWord

    elif not secondWord:
        newWord = firstWord

    else:
        newWord = secondWord

    wordSets[i] = ''.join(newWord) # write the new word to the original list

minOverlap = min(nOverlaps) # find the minimum overlap

########## OUTPUT ##########

print('\nThe word combination occurs at a minimum overlap of {0}: {1}\n'.format(minOverlap, ''.join(wordSets)))