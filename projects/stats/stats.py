import math

class Stats:
    def __init__(self, data):
        self.data = data

    def mean(self):
        try:
            _mean_ = self._mean_

        except:
            _mean_ = sum(self.data) / len(self.data)
            self._mean_ = _mean_

        return _mean_

    def median(self):
        try:
            _median_ = self._median_

        except:
            sortedArray = sorted(self.data)
            i = len(self.data) // 2

            if len(self.data) % 2 == 0:
                _median_ = (sortedArray[i] + sortedArray[i + 1]) / 2

            else:
                _median_ = sortedArray[i]

            self._median_ = _median_

        return _median_

    def mode(self):
        try:
            _mode_ = self._mode_

        except:
            groupedArray = {}

            for x in self.data:
                if x not in groupedArray:
                    groupedArray[x] = 1

                else:
                    groupedArray[x] += 1

            maxFreq = max(groupedArray.values())
            _mode_ = [k for (k, v) in groupedArray.items() if v == maxFreq]
            self._mode_ = _mode_

        return _mode_

    def stdDev(self):
        try:
            _stdDev_ = self._stdDev_

        except:
            try:
                _mean_ = self._mean_

            except:
                _mean_ = self.mean()

            data = [(x - _mean_) ** 2 for x in self.data]
            data = sum(data) / (len(data) - 1)
            _stdDev_ = math.sqrt(data)
            self._stdDev_ = _stdDev_

        return _stdDev_

    def variance(self):
        try:
            _variance_ = self._variance_

        except:
            try:
                _stdDev_ = self._stdDev_

            except:
                _stdDev_ = self.stdDev()

            _variance_ = _stdDev_ ** 2
            self._variance_ = _variance_

        return _variance_