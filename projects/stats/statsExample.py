from stats import Stats
import os

path = os.path.dirname(__file__)
filePath = r'{0}\statsData.txt'.format(path)

with open(filePath, 'r') as f:
    data = f.readlines()

data = [float(x) for x in data]
data = Stats(data)

mean = data.mean()
median = data.median()
modes = data.mode()
stdDev = data.stdDev()
variance = data.variance()

modes = ['{0:.5f}'.format(mode) for mode in modes]

modes = ', '.join(modes)

print('mean:                |  {0:.5f}'.format(mean))
print('median:              |  {0:.5f}'.format(median))
print('mode(s):             |  {0}'.format(modes))
print('standard deviation:  |  {0:.5f}'.format(stdDev))
print('variance:            |  {0:.5f}'.format(variance))