class Cell:
    def __init__(self, val, rLocation):
        self.val = val
        self.rLocation = (r0, r1) = rLocation

        # create new index format
        self.bLocation = (3 * (r0 // 3) + (r1 // 3), 3 * (r0 % 3) + (r1 % 3))

    # get the cell's region
    def getRegions(self, puzzle):
        rowData = puzzle.rowData
        colData = puzzle.colData
        boxData = puzzle.boxData

        # get row and box indices
        (r0, r1) = self.rLocation
        (b0, b1) = self.bLocation

        # get cell's region excluding current cell
        rowRegion = rowData[r0][:r1] + rowData[r0][r1 + 1:]
        colRegion = colData[r1][:r0] + colData[r1][r0 + 1:]
        boxRegion = boxData[b0][:b1] + boxData[b0][b1 + 1:]

        self.regions = [rowRegion, colRegion, boxRegion]