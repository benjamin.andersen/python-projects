from cell import Cell

class Puzzle:
    def __init__(self, state):
        self.state = state
        self.unsolvedCells = []
        self.rowData = [[0] * 9 for _ in range(9)]
        self.colData = [[0] * 9 for _ in range(9)]
        self.boxData = [[0] * 9 for _ in range(9)]

        # build initial puzzle data
        self.getRegionData()
        self.buildNotes()

    def getRegionData(self):
        state = self.state

        for (r0, row) in enumerate(state):
            for (r1, n) in enumerate(row):
                # get the index and number
                rLocation = (r0, r1)
                n = state[r0][r1]

                # create cell obj
                cell = Cell(n, rLocation)

                # get box index format
                (b0, b1) = cell.bLocation

                # put cell into new lists
                self.rowData[r0][r1] = cell
                self.colData[r1][r0] = cell
                self.boxData[b0][b1] = cell

    def buildNotes(self):
        nums = range(1, 10)
        rowData = self.rowData

        for row in rowData:
            for cell in row:
                cell.getRegions(self)

                if not cell.val:
                    # add to unsolvedCells
                    self.unsolvedCells.append(cell)

                    # flatten cell.regions and convert to cell values
                    regions = cell.regions
                    region = [c.val for region in regions for c in region]

                    # get possible vals for cell
                    cell.val = {n for n in nums if n not in region}

    def update(self, n, cell):
        (r0, r1) = cell.rLocation

        # update cell and puzzle
        cell.val = n
        self.unsolvedCells.remove(cell)
        self.state[r0][r1] = n

        # remove n from regions
        regions = cell.regions

        for region in regions:
            for c in region:
                if type(c.val) == set:
                    c.val.discard(n)

        cell.regions = regions

    def verify(self):
        rowData = self.rowData
        colData = self.colData
        boxData = self.boxData

        # check rows, cols, and boxes for duplicates
        for row in rowData:
            _row = []

            # check for cells with no possible vals
            for cell in row:
                val = cell.val

                if not val:
                    return False

                elif type(val) == int:
                    _row.append(val)

            # difference means duplicates
            if len(_row) - len(set(_row)):
                return False

        for col in colData:
            _col = []

            for cell in col:
                val = cell.val

                if not val:
                    return False

                elif type(val) == int:
                    _col.append(val)

            if len(_col) - len(set(_col)):
                return False

        for box in boxData:
            _box = []

            for cell in box:
                val = cell.val

                if not val:
                    return False

                elif type(val) == int:
                    _box.append(val)

            if len(_box) - len(set(_box)):
                return False

        return True

    def solved(self):
        for row in self.state:
            if 0 not in row:
                if not len(row) - len(set(row)):
                    return True

        return False