import os, sys
from json import loads, dumps
from copy import copy, deepcopy
from cell import Cell
from puzzle import Puzzle

########## FUNCTIONS ##########

# general
def processFile(fileName, data = None):
    # write
    if data:
        for (i, row) in enumerate(data):
            data[i] = '    ' + str(row)

            if i < 8:
                data[i] += ','

        data = '[\n' + '\n'.join(data) + '\n]'

        with open(fileName, 'w') as f:
            f.write(data)

    # read
    else:
        with open(fileName, 'r') as f:
            data = f.read()

        return loads(data)

# reduction
def reducePuzzle(puzzle):
    update = True

    while update:
        update = False

        for cell in copy(puzzle.unsolvedCells):
            possibleNums = cell.val

            # if there is only one possible number
            if len(possibleNums) == 1:
                puzzle.update(possibleNums.pop(), cell)
                update = True

            else:
                regions = cell.regions

                for region in regions:
                    # filter out solved cells into vals, flatten
                    region = [c.val for c in region if type(c.val) == set]
                    region = [n for cVals in region for n in cVals]

                    # find n in possibleNums not in region
                    for n in possibleNums:
                        if n not in region:
                            puzzle.update(n, cell)
                            update = True
                            break

                    else: continue
                    break

# solve
def solve(puzzle):
    for (i, cell) in enumerate(puzzle.unsolvedCells):
        possibleNums = cell.val

        for n in possibleNums:
            # change the memory location
            cell = puzzle.unsolvedCells[i]

            # make deepcopies of old data
            state = deepcopy(puzzle.state)
            unsolvedCells = deepcopy(puzzle.unsolvedCells)
            rowData = deepcopy(puzzle.rowData)
            colData = deepcopy(puzzle.colData)
            boxData = deepcopy(puzzle.boxData)

            # try the new location with reduction
            puzzle.update(n, cell)
            reducePuzzle(puzzle)

            # if the puzzle is not solveable, undo
            if not puzzle.verify():
                # this changes memory location
                puzzle.state = state
                puzzle.unsolvedCells = unsolvedCells
                puzzle.rowData = rowData
                puzzle.colData = colData
                puzzle.boxData = boxData
                continue

            # if the puzzle is not solved, recursion
            elif not puzzle.solved():
                solve(puzzle)

            return

########## MAIN FUNCTION ##########

if __name__ == '__main__':
    # input puzzle
    inputFile = os.path.join(sys.path[0], r'sudokuPuzzle.json')
    puzzle = processFile(inputFile)

    # create puzzle obj
    puzzle = Puzzle(puzzle)

    # perform first note reduction algorithm
    reducePuzzle(puzzle)

    # solve puzzle
    solve(puzzle)

    # output solved puzzle
    outputFile = os.path.join(sys.path[0], r'solved.json')
    processFile(outputFile, puzzle.state)