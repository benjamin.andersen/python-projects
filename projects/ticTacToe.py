import random, time

########## CLASS ##########

class Board:
    def __init__(self):
        # randomly pick first player
        if random.randint(0, 1):
            print('It\'s heads! You are X.')
            self.userChar = 'X'; self.compChar = 'O'
            self.turn = True

        else:
            print('It\'s tails! You are O.')
            self.userChar = 'O'; self.compChar = 'X'
            self.turn = False

        self.nTurns = 0
        self.corners = [(0, 0), (0, 2), (2, 0), (2, 2)]
        self.edges = [(0, 1), (1, 0), (1, 2), (2, 1)]
        self.state = [
            [' ', ' ', ' '],
            [' ', ' ', ' '],
            [' ', ' ', ' ']
        ]

    def display(self):
        print(' {0} | 1 | 2 | 3'.format(self.userChar))
        print('---------------')

        for (i, row) in enumerate(self.state):
            print(' {0} | {1}'.format(i + 1, ' | '.join(row)))
            if i < 2: print('---------------')

    def move(self, location, move):
        # unpack location and make move
        (i, j) = location
        self.state[i][j] = move

    def findMoves(self):
        # find every empty location
        return [(i, j) for (i, row) in enumerate(self.state) for (j, val) in enumerate(row) if val == ' ']

    def rand(self, corner):
        state = self.state

        # get unoccupied corners
        if corner:
            unoccupied = [(i, j) for (i, j) in self.corners if state[i][j] == ' ']

        # get unoccupied edges
        else:
            unoccupied = [(i, j) for (i, j) in self.edges if state[i][j] == ' ']

        # pick a random space
        return unoccupied[random.randint(0, len(unoccupied) - 1)]

    def gameOver(self):
        # check to see if a player won
        if (self.crossedRow() or
        self.crossedCol() or
        self.crossedDiag()):
            return True

        return False

    def crossedRow(self):
        # check every row for win
        for row in self.state:
            if (row[0] != ' ' and
            row[0] == row[1] and
            row[1] == row[2]):
                return True

        return False

    def crossedCol(self):
        state = self.state

        # check every col for win
        for i in range(3):
            if (state[0][i] != ' ' and
            state[0][i] == state[1][i] and
            state[1][i] == state[2][i]):
                return True

        return False

    def crossedDiag(self):
        state = self.state

        # check every diag for win
        if state[1][1] != ' ':
            if (state[1][1] == state[0][0] and
            state[0][0] == state[2][2]):
                return True

            elif (state[1][1] == state[0][2] and
            state[0][2] == state[2][0]):
                return True

        return False

########## FUNCTIONS ##########

# driver code
def playGame():
    # set random seed based on timestamp then reset based on rand()
    random.seed(time.time()); random.seed(random.random())

    # make computer seem like its deciding
    print('---------- Tic Tac Toe ----------\n')
    print('Let\'s flip a coin to see who goes first ...')
    time.sleep(1)

    # initialize board and play game
    board = Board()
    while board.nTurns < 9 and not board.gameOver(): nextTurn(board)

    # determine the winner
    if board.gameOver():
        if board.turn: print('\n---------- You Lose ----------\n')
        else: print('\n---------- You Win ----------\n')

    else: print('\n---------- It\'s a Tie ----------\n')
    board.display()

# move
def nextTurn(board):
    turn = board.turn
    nTurns = board.nTurns

    # user goes next
    if turn:
        if nTurns: print('\n---------- Next Turn ----------\n')
        else: print('\n---------- First Turn ----------\n')

        # show current board
        board.display()

        # get user input for move
        move = userInput(board)
        board.move(move, board.userChar)

    else:
        # determine move to make
        move = determineMove(board)
        board.move(move, board.compChar)

    # swap turns, increment
    board.turn = not turn; board.nTurns += 1

def determineMove(board):
    nTurns = board.nTurns
    compChar = board.compChar
    userChar = board.userChar
    corners = board.corners
    edges = board.edges
    state = board.state

    # first turn, pick random corner
    if not nTurns:
        return board.rand(True)

    # second turn
    elif nTurns == 1:
        # user didn't choose center
        if state[1][1] == ' ':
            return (1, 1)

        return board.rand(True)

    # third turn
    elif nTurns == 2:
        # find comp's first move
        for (i, row) in enumerate(state):
            for (j, val) in enumerate(row):
                if val == compChar: break

            else: continue
            break

        # check the opposite corner for player's move
        (i, j) = corners[3 - corners.index((i, j))]
        if state[i][j] == userChar:
            return board.rand(True)

    # check compWin
    if (compWin := findWin(board, compChar)):
        return compWin

    # check userWin
    elif (userWin := findWin(board, userChar)):
        return userWin

    # check userForks
    elif (userForks := findForks(board, userChar)):
        leng = len(userForks)

        if leng >= 2:
            # check for multiple forks
            if leng > 2:
                # find all corners overlapping a fork
                overlap = [fork for fork in userForks if fork in corners]

            # check for two forks
            elif leng == 2:
                # find all edges overlapping a fork
                overlap = [fork for fork in userForks if fork in edges]

            if overlap:
                return overlap[random.randint(0, len(overlap) - 1)]

            return board.rand(False)

        return userForks[0]

    # check compForks
    elif (compForks := findForks(board, compChar)):
        return compForks[random.randint(0, len(compForks) - 1)]

    # check compAttack
    elif (compAttack := findAttack(board, compChar)):
        return compAttack

    # there are no good moves,
    # pick a random move
    moves = board.findMoves()
    return moves[random.randint(0, len(moves) - 1)]

# comp AI
def findWin(board, playerChar):
    # get all legal moves and wins
    moves = board.findMoves()
    wins = []

    for move in moves:
        # make temp move
        board.move(move, playerChar)

        # if player wins, append move
        if board.gameOver():
            wins.append(move)

        # undo temp move
        board.move(move, ' ')

    # check if there are wins
    if wins:
        return wins[random.randint(0, len(wins) - 1)]

    return ()

def findAttack(board, playerChar):
    # get all legal moves and attacks
    moves = board.findMoves()
    attacks = []

    # check each board state for attacks
    for move in moves:
        # make temp move, get new legal moves
        board.move(move, playerChar)
        _moves_ = board.findMoves()

        for _move_ in _moves_:
            # make temp move
            board.move(_move_, playerChar)

            # add attack
            if board.gameOver():
                attacks.append(move)

            # undo temp moves
            board.move(_move_, ' ')

        board.move(move, ' ')

    # check if there are attacks
    if attacks:
        return attacks[random.randint(0, len(attacks) - 1)]

    return ()

def findForks(board, playerChar):
    # get all legal moves and forks
    moves = board.findMoves()
    forks = []

    # check each board state for forks
    for move in moves:
        numWins = 0

        # make temp move, get new legal moves
        board.move(move, playerChar)
        _moves_ = board.findMoves()

        for _move_ in _moves_:
            # make temp move
            board.move(_move_, playerChar)

            # increment numWins
            if board.gameOver():
                numWins += 1

            # undo temp move
            board.move(_move_, ' ')

        # append fork move
        if numWins >= 2:
            forks.append(move)

        # undo temp move
        board.move(move, ' ')

    return forks

# general
def userInput(board):
    err2 = True
    state = board.state
    print('\nMake your move ...')

    # make sure indices don't
    # point to chosen location
    while err2:
        err1 = True

        # get valid row input
        while err1:
            try:
                i = int(input('Row (1 to 3): '))
                err1 = (i < 1) or (i > 3)

                if err1:
                    print('[Error] Invalid index!')

            except:
                print('[Error] Bad input!')

        err1 = True

        # get valid col input
        while err1:
            try:
                j = int(input('Column (1 to 3): '))
                err1 = (j < 1) or (j > 3)

                if err1:
                    print('[Error] Invalid index!')

            except:
                print('[Error] Bad input!')

        # decrement then validate
        i -= 1; j -= 1
        err2 = (state[i][j] != ' ')

        if err2:
            print('[Error] Index occupied!')

    return (i, j)

########## MAIN FUNCTION ##########

if __name__ == '__main__':
    playAgain = 'y'

    while (playAgain == 'y'):
        err = True
        print(); playGame(); print()

        while err:
            playAgain = input('Would you like to play again? (y / n): ')
            err = (playAgain != 'y' and playAgain != 'n')

            if err:
                print('[Error] Bad input!')